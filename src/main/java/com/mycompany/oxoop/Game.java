/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxoop;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
public class Game {

    private Player o;
    private Player x;
    private Board board;
    private int row;
    private int col;
    Scanner s = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');

    }

    public void newBoard() {
        this.board = new Board(o, x);

    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char[][] table = board.getTable();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col: ");
            row = s.nextInt();
            col = s.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }
    
    public boolean isFinish(){
        if(board.isDraw() || board.isWin()){
            return true;
        }
        return false;
    }
    
    public void showResult(){
        if(board.isDraw()) {
            System.out.println(">>>Draw<<<");
        }else if(board.isWin()){
            System.out.println(">>>"+board.getCurrentPlayer().getSymbol()+ " Win<<<");
        }
    }
    
    public void showStat(){
        System.out.println(o);
        System.out.println(x);
    }
}
