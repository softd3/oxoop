/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxoop;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
public class OXOOP {
    static char decide;
    
    public static boolean playMore(){
        if(decide == 'y'){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if(game.isFinish()){
                game.showTable();
                game.showResult();
                game.showStat();
                playOrNot(s);
                if(playMore()){
                    game.newBoard();
                }else{
                    break;
                }
            }
        }

    }

    private static void playOrNot(Scanner s) {
        System.out.println("You want to play 1 more round (y / n) ?");
        decide = s.next().charAt(0);
    }
}
