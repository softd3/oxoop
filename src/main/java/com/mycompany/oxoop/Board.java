/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxoop;

/**
 *
 * @author Aritsu
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    public boolean win = false;
    public boolean draw = false;
    
    
    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    public void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }

    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        
        if(isWin() || isDraw()) return false;
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            System.out.println("Please input row and col 1-3");
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            System.out.println("That position has been already set Please try again");
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        
        if(checkWin(row, col)){
            updateStat();
            this.win = true;
            return true;
        }
        if(checkDraw()){
            updateDraw();
            this.draw = true;
            return true;
        }

        switchPlayer();
        return true;
    }
    
    private void updateDraw() {
        o.draw();
        x.draw();
    }

    private void updateStat() {
        if(this.currentPlayer ==o){
            o.win();
            x.lose();
        }else{
            x.win();
            o.lose();
        }
    }
    
    public  boolean checkWin(int row , int col) {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkX()) {
            return true;
        }

        return false;
    }

    public  boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public  boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;

    }

    public  boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public  boolean checkX1() { // 11 , 22 , 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public  boolean checkX2() { //13 , 22 , 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }

        return true;
    }

    public  boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

}
